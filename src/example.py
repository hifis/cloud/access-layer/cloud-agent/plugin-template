import logging
import uuid
from random import randint

from helmholtz_cloud_agent.core.main import HCAApplication
from helmholtz_cloud_agent.messages import (
    ResourceAllocateV1,
    ResourceCreatedV1,
)
from pydantic import BaseModel, Field, TypeAdapter, ValidationError

# Global logger
logging.basicConfig(
    format="%(asctime)s\t%(levelname)s\t%(filename)s\t%(funcName)s\t%(message)s"
)


logger = logging.getLogger("hca")


class ChatTeamAlreadyExistsError(Exception):
    def __init__(
        self: "ChatTeamAlreadyExistsError",
        folder_name: str,
        message: str = "Chat team already exists",
    ) -> None:
        self.folder_name = folder_name
        self.message = message
        super().__init__(self.message)

    def __str__(self: "ChatTeamAlreadyExistsError") -> str:
        return f"{self.message}: {self.folder_name}"


class ResourcesNotAvailableError(Exception):
    def __init__(
        self: "ResourcesNotAvailableError",
        ram: int,
        cpu: int,
        storage: int,
        message: str = "The requested resources are not available",
    ) -> None:
        self.ram = ram
        self.cpu = cpu
        self.storage = storage
        self.message = message
        super().__init__(self.message)

    def __str__(self: "ResourcesNotAvailableError") -> str:
        return f"{self.message}: {self.cpu}, {self.ram}, {self.storage}"


class UnknownResourceSpecTypeError(Exception):
    def __init__(
        self: "UnknownResourceSpecTypeError",
        spec_type: str,
        message: str = "Unknown resource spec type",
    ) -> None:
        self.spec_type = spec_type
        self.message = message
        super().__init__(self.message)

    def __str__(self: "UnknownResourceSpecTypeError") -> str:
        return f"{self.message}: {self.spec_type}"


class ResourceSpecTypeEmptyError(Exception):
    def __init__(
        self: "ResourceSpecTypeEmptyError",
        message: str = "Resource spec type is empty. Request cannot be handled.",
    ) -> None:
        self.message = message
        super().__init__(self.message)

    def __str__(self: "ResourceSpecTypeEmptyError") -> str:
        return f"{self.message}"


class ChatTeamSpecV1(BaseModel):
    team_name: str = Field(..., title="Name of the new team")
    invite_only: bool = Field(..., title="Make invites necessary")


class ComputeResourceSpecV1(BaseModel):
    cpu: int = Field(..., title="Requested CPU")
    ram: int = Field(..., title="Requested RAM")
    storage: int = Field(..., title="Requested Storage")


app = HCAApplication()


@app.handle(message_type="ResourceAllocateV1")  # type: ignore[misc]
def resource_allocate_v1(
    correlation_id: str, payload: ResourceAllocateV1
) -> ResourceCreatedV1:
    """Example message handler for a ResourceAllocateV1 message"""
    if not payload.type:
        raise ResourceSpecTypeEmptyError

    logger.info(
        "Provision resource of type '%s'. Message correlation_id: '%s'",
        payload.type,
        correlation_id,
    )
    random_threshold = 3
    dataclass_validator: (
        TypeAdapter[ChatTeamSpecV1] | TypeAdapter[ComputeResourceSpecV1]
    )
    spec: ChatTeamSpecV1 | ComputeResourceSpecV1
    try:
        if payload.type == "ChatTeamSpecV1":
            dataclass_validator = TypeAdapter(ChatTeamSpecV1)
            spec = dataclass_validator.validate_python(payload.specification)
            logger.info(" [Resource specification]")
            logger.info(f" -> Type: {payload.type}")
            logger.info(" -> Team name: {spec.team_name}")
            logger.info(" -> Invite Only: {spec.invite_only}")
            if randint(0, 10) < random_threshold:  # nosec
                raise ChatTeamAlreadyExistsError(spec.team_name)
        elif payload.type == "ComputeResourceSpecV1":
            dataclass_validator = TypeAdapter(ComputeResourceSpecV1)
            spec = dataclass_validator.validate_python(payload.specification)
            logger.info(" [Resource specification]")
            logger.info(" -> Type: {payload.type}")
            logger.info(" -> CPU: {spec.cpu}")
            logger.info(" -> RAM: {spec.ram}")
            logger.info(" -> Storage: {spec.storage}")
            if randint(0, 10) < random_threshold:  # nosec
                raise ResourcesNotAvailableError(
                    spec.ram,
                    spec.cpu,
                    spec.storage,
                )
        else:
            logger.error("Unknown resource spec '%s'", payload.type)
            raise UnknownResourceSpecTypeError(payload.type)
    except ValidationError:
        logger.exception("cannot validate request specification")
    return ResourceCreatedV1(id=str(uuid.uuid4()))


app.run()
