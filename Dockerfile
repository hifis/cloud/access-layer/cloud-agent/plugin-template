FROM registry.hub.docker.com/library/python:3.13.2-slim-bookworm@sha256:f3614d98f38b0525d670f287b0474385952e28eb43016655dd003d0e28cf8652 as uv

# Update and install curl
RUN apt-get update && apt-get -y dist-upgrade

# Install uv for python
RUN pip install --upgrade pip && pip install uv

RUN mkdir /hca
WORKDIR /hca

COPY uv.lock pyproject.toml /hca/

RUN uv sync --no-dev --locked

FROM registry.hub.docker.com/library/python:3.13.2-slim-bookworm@sha256:f3614d98f38b0525d670f287b0474385952e28eb43016655dd003d0e28cf8652

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -y dist-upgrade

ADD src /opt/hca
COPY --from=uv /hca/.venv /opt/hca/venv

RUN useradd --system --no-create-home --home-dir /opt/hca hca && chown -R hca: /opt/hca
WORKDIR /opt/hca
USER hca

EXPOSE 80

# adapt the python file to point to your HCA
ENTRYPOINT [ "/bin/bash", "-c", "/opt/hca/venv/bin/python example.py" ]
