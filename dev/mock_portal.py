import json
import os
import uuid
from time import sleep

import pika
import typer
from helmholtz_cloud_agent.messages import ResourceAllocateV1
from pydantic import TypeAdapter
from typing_extensions import Annotated

app = typer.Typer(no_args_is_help=True)


def _setup_connection() -> pika.ConnectionParameters:
    username = os.environ.get("HCA_USERNAME")
    password = os.environ.get("HCA_PASSWORD")
    hostname = os.environ.get("HCA_RABBITMQ_HOSTNAME")
    credentials = pika.PlainCredentials(username, password)
    pika_params = pika.ConnectionParameters(
        host=hostname,
        port=5672,
        credentials=credentials,
        virtual_host="test-provider",
    )
    return pika_params


def _send_message(
    pika_params: pika.ConnectionParameters,
    properties: pika.BasicProperties,
    message: bytes,
) -> None:
    connection = pika.BlockingConnection(parameters=pika_params)
    channel = connection.channel()
    channel.basic_publish(
        exchange="",
        routing_key="to_hca",
        body=message,
        properties=properties,
    )


def _on_message(
    channel: pika.adapters.blocking_connection.BlockingChannel,
    method: pika.spec.Basic.Deliver,
    props: pika.BasicProperties,
    body: bytes,
) -> None:
    print(f"{props.type}:\n{json.dumps(json.loads(body), indent=4)}")
    channel.basic_ack(delivery_tag=method.delivery_tag)


@app.command()
def send_ping() -> None:
    """
    Send a PingV1 message to HCA and wait for response.
    """
    pika_params = _setup_connection()
    connection = pika.BlockingConnection(parameters=pika_params)
    channel = connection.channel()
    properties = pika.BasicProperties(
        type="PingV1",
        headers={"service_id": str(os.environ.get("HCA_SERVICE_ID"))},
    )
    _send_message(pika_params=pika_params, properties=properties, message="".encode())
    while True:
        method, props, body = channel.basic_get("to_portal")
        if method:
            break
    if method:
        _on_message(channel=channel, method=method, props=props, body=body)


@app.command(no_args_is_help=True)
def send_resource_allocate(
    payload_file: Annotated[
        str, typer.Argument(help="File containing the JSON payload")
    ],
) -> None:
    """
    Send a ResourceAllocateV1 message with payload from a passed file to HCA and wait for response.
    """
    with open(payload_file, "r") as f:
        payload_json = json.dumps(json.load(f))
        dataclass_validator = TypeAdapter(ResourceAllocateV1)
        payload = dataclass_validator.validate_json(payload_json)

    pika_params = _setup_connection()
    connection = pika.BlockingConnection(parameters=pika_params)
    channel = connection.channel()
    properties = pika.BasicProperties(
        type="ResourceAllocateV1",
        correlation_id=str(uuid.uuid4()),
        headers={"service_id": str(os.environ.get("HCA_SERVICE_ID"))},
    )

    _send_message(
        pika_params=pika_params,
        properties=properties,
        message=json.dumps(payload.model_dump(mode="json")).encode(),
    )

    # wait shortly for the HCA to respond
    sleep(1)
    method, props, body = channel.basic_get("to_portal")
    if method:
        _on_message(channel=channel, method=method, props=props, body=body)


if __name__ == "__main__":
    app()
