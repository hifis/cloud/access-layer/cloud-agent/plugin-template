# Helmholtz Cloud Agent Template

This repository provides a template that can be used to develop your own Helmholtz Cloud Agent (HCA). Simply fork this repository and start developing.

## Getting started

This repository provides a devcontainer setup that can be used with VSCode to start a development environment together with a RabbitMQ service for local testing. There is a fully functional sample Helmholtz Cloud Agent that connects to the provided RabbitMQ instance:

```
poetry run python src/example.py
```

In a separate shell you can try sending messages to the sample Helmholtz Cloud Agent. This repository includes a simple script that mocks the interaction from the Cloud Portal with the Helmholtz Cloud Agent.

You can either send a `PingV1` message to check if the Helmholtz Cloud Agent is running correctly:

```
poetry run python dev/mock_portal.py send-ping
```

Or you can send a `ResourceAllocateV1` message. You have to pass a file containing the JSON payload for the request. You can find two examples in the the [payloads](./payloads/) folder:

```
poetry run python dev/mock_portal.py send-resource-allocate payloads/ComputeResourceSpecV1.json
```

## Develop your own Helmholtz Cloud Agent

You can take the sample Helmholtz Cloud Agent at src/example.py as a starting point to develop your own Helmholtz Cloud Agent. It is a simple Helmholtz Cloud Agent that handles only `ResourceAllocateV1` requests and randomly returns successful and failed messages back to the queue.

## Adding additional dependencies

This project uses poetry. If you need additional dependencies just add them with:

```
poetry add <depname>
```

## Deploy your Helmholtz Cloud Agent

This repository provides a Dockerfile that can be used to package and deploy your Helmholtz Cloud Agent. Adapt the `ENTRYPOINT` to point to your Helmholtz Cloud Agent.
